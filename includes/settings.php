<?php
/**
 * @package   Awesome Support Theme Features Settings
 * @author    ThemeAvenue <web@themeavenue.net>
 * @license   GPL-2.0+
 * @link      http://themeavenue.net
 * @copyright 2014 ThemeAvenue
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

add_filter( 'astf_plugin_settings', 'astf_settings' );
/**
 * Declare all theme options
 *
 * @since 1.0.0
 *
 * @param array $settings Theme settings
 *
 * @return array
 */
function astf_settings( $settings ) {

	$default = array(
		'general' => array(
			'name'    => __( 'General', 'as-features' ),
			'options' => array(
				array(
					'name'    => __( 'Archive Tagline', 'as-features' ),
					'id'      => 'archive_tagline',
					'type'    => 'textarea',
					'desc'    => esc_html__( 'Tagline to show at the top of the downloads archive page.', 'as-features' ),
					'default' => ''
				),
				array(
					'name'     => __( 'Terms Page', 'as-features' ),
					'id'       => 'terms_page',
					'type'     => 'select',
					'multiple' => false,
					'desc'     => esc_html__( 'Which page contains the terms of service?', 'as-features' ),
					'options'  => astf_list_pages(),
					'default'  => ''
				),
				array(
					'name'    => __( 'Plugin Download URL', 'as-features' ),
					'id'      => 'plugin_download_url',
					'type'    => 'text',
					'desc'    => esc_html__( 'Link to the plugin zip file.', 'as-features' ),
					'default' => 'http://downloads.wordpress.org/plugin/awesome-support.latest-stable.zip'
				),
			)
		),
		'homepage' => array(
			'name'    => __( 'Homepage', 'as-features' ),
			'options' => array(
				array(
					'name'    => __( 'Intro Video ID', 'as-features' ),
					'id'      => 'intro_video_id',
					'type'    => 'text',
					'desc'    => esc_html__( 'ID of the Youtube introduction video.', 'as-features' ),
					'default' => ''
				),
				array(
					'name'    => __( 'Hero Image', 'as-features' ),
					'id'      => 'hero_image',
					'type'    => 'upload',
					'desc'    => esc_html__( 'The image used as background of the hero section.', 'as-features' ),
					'default' => ''
				),
				array(
					'name'    => __( 'Homepage Reviews', 'as-features' ),
					'id'      => 'homepage_reviews',
					'type'    => 'text',
					'desc'    => __( 'The shortcode to display the reviews. <a target="_blank" href="https://github.com/ThemeAvenue/Plugin-Reviews/wiki/Shortcode-Attributes">View all attributes</a>.', 'as-features' ),
					'default' => '[wr_reviews plugin_slug="awesome-support"]'
				),
				array(
					'name'    => __( 'Featured Add-On #1', 'as-features' ),
					'id'      => 'featured_addon_1',
					'type'    => 'select-posts',
					'post_status' => 'published',
					'post_type' => 'download',
					'default' => ''
				),
				array(
					'name'    => __( 'Featured Add-On #2', 'as-features' ),
					'id'      => 'featured_addon_2',
					'type'    => 'select-posts',
					'post_status' => 'published',
					'post_type' => 'download',
					'default' => '',
					'desc'    => esc_html__( 'This one has the "Recommended" ribbon.', 'as-features' ),
				),
				array(
					'name'    => __( 'Featured Add-On #3', 'as-features' ),
					'id'      => 'featured_addon_3',
					'type'    => 'select-posts',
					'post_status' => 'published',
					'post_type' => 'download',
					'default' => ''
				),
				array(
					'name'    => __( 'Featured Add-On #4', 'as-features' ),
					'id'      => 'featured_addon_4',
					'type'    => 'select-posts',
					'post_status' => 'published',
					'post_type' => 'download',
					'default' => ''
				),				
			)
		),
	);

	return array_merge( $settings, $default );

}