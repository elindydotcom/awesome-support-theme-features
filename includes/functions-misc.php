<?php
/**
 * @package   Awesome Support Theme Features Settings
 * @author    ThemeAvenue <web@themeavenue.net>
 * @license   GPL-2.0+
 * @link      http://themeavenue.net
 * @copyright 2014 ThemeAvenue
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Get plugin option.
 *
 * @param  string      $option  Option to look for
 * @param  bool|string $default Value to return if the requested option doesn't exist
 *
 * @return mixed           Value for the requested option
 * @since  1.0.0
 */
function astf_get_option( $option, $default = false ) {

	$options = maybe_unserialize( get_option( 'as-features_options', array() ) );

	/* Return option value if exists */
	$value = isset( $options[ $option ] ) ? $options[ $option ] : $default;

	return apply_filters( 'astf_option_' . $option, $value );
}

/**
 * List all site pages.
 *
 * @return array List of pages in an array of the form page_id => page_title
 * @since  3.0.0
 */
function astf_list_pages() {

	$list  = array( '' => __( 'None', 'as-features' ) );

	$args  = array(
		'post_type'              => 'page',
		'post_status'            => 'publish',
		'order'                  => 'DESC',
		'orderby'                => 'page_title',
		'posts_per_page'         => - 1,
		'no_found_rows'          => false,
		'cache_results'          => false,
		'update_post_term_cache' => false,
		'update_post_meta_cache' => false,

	);
	$pages = new WP_Query( $args );

	if ( ! empty( $pages->posts ) ) {
		foreach ( $pages->posts as $page ) {
			$list[ $page->ID ] = apply_filters( 'the_title', $page->post_title );
		}
	}

	return apply_filters( 'astf_pages_list', $list );

}