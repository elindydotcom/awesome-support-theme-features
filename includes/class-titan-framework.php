<?php
/**
 * @package   Awesome Support Theme Features Titan Framework Loader
 * @author    ThemeAvenue <web@themeavenue.net>
 * @license   GPL-2.0+
 * @link      http://themeavenue.net
 * @copyright 2014 ThemeAvenue
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

final class ASTF_Titan {

	/**
	 * Instance of this class.
	 *
	 * @since    1.0.0
	 * @var      object
	 */
	protected static $instance = null;

	public function __construct() {
		add_action( 'after_setup_theme', array( $this, 'load_titan_framework' ), 12 );
	}

	/**
	 * Load Titan Framework.
	 *
	 * @link   http://www.titanframework.net/embedding-titan-framework-in-your-project/
	 * @since  1.0.0
	 */
	public function load_titan_framework() {

		/*
		 * When using the embedded framework, use it only if the framework
		 * plugin isn't activated.
		 */

		// Don't do anything when we're activating a plugin to prevent errors
		// on redeclaring Titan classes
		if ( ! empty( $_GET['action'] ) && ! empty( $_GET['plugin'] ) ) {
			if ( $_GET['action'] == 'activate' ) {
				return;
			}
		}

		// Check if the framework plugin is activated
		$useEmbeddedFramework = true;
		$activePlugins = get_option('active_plugins');
		if ( is_array( $activePlugins ) ) {
			foreach ( $activePlugins as $plugin ) {
				if ( is_string( $plugin ) ) {
					if ( stripos( $plugin, '/titan-framework.php' ) !== false ) {
						$useEmbeddedFramework = false;
						break;
					}
				}
			}
		}

		// Use the embedded Titan Framework
		if ( $useEmbeddedFramework && ! class_exists( 'TitanFramework' ) ) {
			require_once( AS_THEME_FEATURES_PATH . 'vendor/gambitph/titan-framework/titan-framework.php' );
		}

		/*
		 * Start your Titan code below
		 */
		$titan = TitanFramework::getInstance( 'as-features' );

		$settings = $titan->createAdminPage( array(
				'name'       => __( 'Theme Options', 'as-features' ),
				'title'      => __( 'Awesome Support Theme Options', 'as-features' ),
				'id'         => 'astf-options',
				'parent'     => 'themes.php',
				'capability' => 'administrator'
			)
		);

		/**
		 * Get plugin core options
		 *
		 * @var (array)
		 * @see  admin/includes/settings.php
		 */
		$options = apply_filters( 'astf_plugin_settings', array() );

		/* Parse options */
		foreach ( $options as $tab => $content ) {

			/* Add a new tab */
			$tab = $settings->createTab( array(
					'name'  => $content['name'],
					'title' => isset( $content['title'] ) ? $content['title'] : $content['name'],
					'id'    => $tab
				)
			);

			/* Add all options to current tab */
			foreach( $content['options'] as $option ) {
				$tab->createOption( $option );
			}

			$tab->createOption( array( 'type' => 'save', ) );

		}

	}

}