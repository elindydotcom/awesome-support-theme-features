This plugin holds all the Awesome Support theme's custom features. That includes all the theme options.

## Adding Theme Options

In order to add theme options, you have to edit the file `includes/settings.php` and add the new options to the options array.

It is possible to add new sections as well. Just refer to the structure in the settings file.

## Getting Theme Options

Getting a theme option is as simple as calling the `astf_get_option()` function. The function takes two parameters: `$option` (the option ID) and `$default` (the default value to return if the option doesn't exist).

### Getting Theme Options from the Theme

In order to get options from within the theme, it is best to use the wrapper function `tas_get_option()`. This function takes the exact same two parameters. It checks for `astf_get_option()` before calling it in order to avoid PHP issues.