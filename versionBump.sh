#!/bin/sh
echo "Working directory: $PWD"
cd $PWD
echo "Please enter the version number (x.x.x): "
read version
echo "You entered: $version"
# stage the files
git add package.json awesome-support-features.php
# commit them
git commit -am "Bump version $version"
# push to master
git push origin master
# create versiont tag
git tag -a $version -m "Bump version $version"
# push tags
git push --tags
echo Press Enter...
read