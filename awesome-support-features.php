<?php
/**
 * @package   Awesome Support Theme Features
 * @author    ThemeAvenue <web@themeavenue.net>
 * @license   GPL-2.0+
 * @link      http://themeavenue.net
 * @copyright 2014 ThemeAvenue
 *
 * @wordpress-plugin
 * Plugin Name:       Awesome Support Theme Features
 * Plugin URI:        http://getawesomesupport.com
 * Description:       Awesome Support theme features plugin.
 * Version:           1.1.4
 * Author:            ThemeAvenue
 * Author URI:        http://themeavenue.net
 * Text Domain:       as-features
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Domain Path:       /languages
 * Bitbucket Plugin URI: https://bitbucket.org/themeavenue/awesome-support-theme-features
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

if ( ! class_exists( 'AS_Theme_Features' ) ):

	/**
	 * Main Awesome Support Features class
	 *
	 * This class is the one and only instance of the plugin. It is used
	 * to load the core and all its components.
	 *
	 * @since 1.0.0
	 */
	final class AS_Theme_Features {

		/**
		 * @var AS_Theme_Features Holds the unique instance of Awesome Support
		 * @since 1.0.0
		 */
		private static $instance;

		/**
		 * Holds our instance of the Titan Framework
		 *
		 * @var ASTF_Titan
		 * @since 1.0.0
		 */
		public static $titan;

		/**
		 * Instantiate and return the unique Awesome Support object
		 *
		 * @since     1.0.0
		 * @return object Awesome_Support Unique instance of Awesome Support
		 */
		public static function instance() {

			if ( ! isset( self::$instance ) && ! ( self::$instance instanceof AS_Theme_Features ) ) {

				self::$instance = new AS_Theme_Features;
				self::$instance->setup_constants();
				self::$instance->includes();

				if ( is_admin() ) {
					self::$titan = new ASTF_Titan();
				}

			}

			return self::$instance;
		}

		/**
		 * Throw error on object clone
		 *
		 * The whole idea of the singleton design pattern is that there is a single
		 * object therefore, we don't want the object to be cloned.
		 *
		 * @since 1.0.0
		 * @return void
		 */
		public function __clone() {
			// Cloning instances of the class is forbidden
			_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'wpas' ), '1.0.0' );
		}

		/**
		 * Disable unserializing of the class
		 *
		 * @since 1.0.0
		 * @return void
		 */
		public function __wakeup() {
			// Unserializing instances of the class is forbidden
			_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'wpas' ), '1.0.0' );
		}

		/**
		 * Setup all plugin constants
		 *
		 * @since 1.0.0
		 * @return void
		 */
		private function setup_constants() {
			define( 'AS_THEME_FEATURES_VERSION', '1.1.4' );
			define( 'AS_THEME_FEATURES_URL', trailingslashit( plugin_dir_url( __FILE__ ) ) );
			define( 'AS_THEME_FEATURES_PATH', trailingslashit( plugin_dir_path( __FILE__ ) ) );
		}

		/**
		 * Include all files
		 *
		 * @since 1.0.0
		 * @return void
		 */
		private function includes() {

			require( AS_THEME_FEATURES_PATH . 'includes/functions-misc.php' );

			if ( is_admin() ) {
				require( AS_THEME_FEATURES_PATH . 'includes/settings.php' );
				require( AS_THEME_FEATURES_PATH . 'includes/class-titan-framework.php' );
			}

		}

	}

endif;

/**
 * The main function responsible for returning the unique Awesome Support instance
 *
 * Use this function like you would a global variable, except without needing
 * to declare the global.
 *
 * @since 1.0.0
 * @return object AS_Theme_Features
 */
function ASTF() {
	return AS_Theme_Features::instance();
}

// Get Awesome Support theme features running
ASTF();