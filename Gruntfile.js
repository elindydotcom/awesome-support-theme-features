/* globals module, require */

module.exports = function (grunt) {

	'use strict';

	// Everything except HTML files
	var filesToBump = [
		'package.json',
		'<%= pkg.main %>'
	];

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		version: {
			packageJSON: {
				src: ['package.json']
			},
			pluginVersion: {
				options: {
					prefix: 'Version:\\s+'
				},
				src: [
					'<%= pkg.main %>'
				]
			},
			pluginConstant: {
				options: {
					prefix: 'define\\(\\s*\'AS_THEME_FEATURES_VERSION\',\\s*\''

				},
				src: ['<%= pkg.main %>']
			}
		},
		gitadd: {
			version: {
				files: {
					src: filesToBump
				}
			}
		},
		gitcommit: {
			version: {
				options: {
					message: 'Bump version to <%= pkg.version %>'
				},
				files: {
					src: filesToBump
				}
			}
		},
		gittag: {
			version: {
				options: {
					tag: '<%= pkg.version %>',
					message: 'Bump version to <%= pkg.version %>'
				}
			}
		},
		gitpush: {
			version: {
				options: {
					remote: 'origin',
					branch: 'master'
				}
			},
			tags: {
				options: {
					remote: 'origin',
					branch: 'master',
					tags: true
				}
			},
		},
	});

	require('load-grunt-tasks')(grunt);

	grunt.registerTask('bump', [
		'version::patch',
		'gitcommit:version',
		'gitpush:version'
	]);

	grunt.registerTask('tag', [
		'gittag:version',
		'gitpush:tags'
	]);

	grunt.registerTask('release', [
		'bump',
		'tag'
	]);

};